
all:
	@$(MAKE) -C native install
	ant

install:
	$(MAKE) all
	cp misc/Lanemu.exe build/Lanemu.exe
	cp misc/Lanemu-legacy.exe build/Lanemu-legacy.exe
	cp misc/lanemu-pkexec build/lanemu-pkexec

clean:
	@$(MAKE) -C native clean
	ant clean