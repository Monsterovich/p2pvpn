/*
    Copyright 2017 Serge Zaitsev
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of App indicator tray icon library for Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRAY_H
#define TRAY_H

#include <jni.h>

#define JFUNC(type, name, ...) JNIEXPORT type JNICALL Java_org_p2pvpn_gui_AppIndicatorTrayIcon_##name(JNIEnv *env, jobject this, ##__VA_ARGS__)

JFUNC(void, addMenu, jobject menu);
JFUNC(void, trayRun, jstring tooltip);
JFUNC(void, showMessage, jstring caption, jstring text);
JFUNC(void, test);

struct tray_menu {
    char *text;
    int disabled;
    int checked;
    int checkbox;

    void (*cb)(struct tray_menu *);

    struct tray_menu *submenu;
    size_t size;

    // jni stuff
    jobject menu_item;
};

struct tray {
    const char *icon;
    const char *tooltip;
    struct tray_menu *menu;
    size_t size;

    // jni stuff
    JNIEnv *env;
    jobject obj;
};

struct notification {
    char *caption;
    char *text;
    char *icon;
};

static void tray_update(struct tray *tray);

#include <gtk/gtk.h>
#include <libayatana-appindicator/app-indicator.h>
#include <libnotify/notify.h>

#define NOTIFICATION_TIMEOUT 5000

static AppIndicator *indicator = NULL;
static int loop_result = 0;

static void _tray_menu_cb(GtkMenuItem *item, gpointer data) {
    (void)item;
    struct tray_menu *m = (struct tray_menu *)data;
    m->cb(m);
}

static gboolean _tray_notification_cb(gpointer data) {
    struct notification *notification = (struct notification *)data;

    if (notify_is_initted()) {
        NotifyNotification *notify_notification = notify_notification_new(
            notification->caption, 
            notification->text, 
            notification->icon
        );

        notify_notification_set_timeout(notify_notification, NOTIFICATION_TIMEOUT);
        notify_notification_show(notify_notification, NULL);

        g_object_unref(G_OBJECT(notify_notification));
    }

    free(notification->caption);
    free(notification->text);
    free(notification->icon);
    free(notification);

    return G_SOURCE_REMOVE;
}

static GtkMenuShell *_tray_menu(struct tray_menu *tray_menu, size_t size) {
    GtkMenuShell *menu = (GtkMenuShell *)gtk_menu_new();
    for (size_t i = 0; i < size; i++) {
        GtkWidget *item;
        struct tray_menu *m = &tray_menu[i];
        if (strcmp(m->text, "-") == 0) {
            item = gtk_separator_menu_item_new();
        }
        else {
            if (m->submenu != NULL) {
                item = gtk_menu_item_new_with_label(m->text);
                gtk_menu_item_set_submenu(GTK_MENU_ITEM(item),
                                          GTK_WIDGET(_tray_menu(m->submenu, m->size)));
            }
            else if (m->checkbox) {
                item = gtk_check_menu_item_new_with_label(m->text);
                gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item), !!m->checked);
            }
            else {
                item = gtk_menu_item_new_with_label(m->text);
            }
            gtk_widget_set_sensitive(item, !m->disabled);
            if (m->cb != NULL) {
                g_signal_connect(item, "activate", G_CALLBACK(_tray_menu_cb), m);
            }
        }
        gtk_widget_show(item);
        gtk_menu_shell_append(menu, item);
    }
    return menu;
}

static int tray_init(struct tray *tray) {
    if (gtk_init_check(0, NULL) == FALSE) {
        return -1;
    }
    indicator = app_indicator_new(tray->tooltip, tray->icon,
                                  APP_INDICATOR_CATEGORY_APPLICATION_STATUS);
    app_indicator_set_status(indicator, APP_INDICATOR_STATUS_ACTIVE);
    app_indicator_set_title(indicator, tray->tooltip);
    if (notify_init(tray->tooltip) == FALSE) {
        printf("notify init failed\n");
    }
    tray_update(tray);
    return 0;
}

static int tray_loop(int blocking) {
    gtk_main_iteration_do(blocking);
    return loop_result;
}

static void tray_update(struct tray *tray) {
    app_indicator_set_icon(indicator, tray->icon);
    // GTK is all about reference counting, so previous menu should be destroyed
    // here
    app_indicator_set_menu(indicator, GTK_MENU(_tray_menu(tray->menu, tray->size)));
}

static void tray_notification_show(struct notification *notification) {
    g_idle_add(_tray_notification_cb, notification);
}

static void tray_exit() { loop_result = -1; }

#endif /* TRAY_H */
