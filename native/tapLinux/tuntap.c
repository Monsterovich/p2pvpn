/*
    Copyright 2008 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#ifdef __FreeBSD__
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_tap.h>
#else
#include <linux/ioctl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#endif

#ifdef __FreeBSD__
#define TUNTAP_DEV "/dev/tap"
#if defined USE_TRANSIENT && !defined TAPSTRANSIENT
#define TUNSTRANSIENT    _IOW('t', 98, int)
#define TAPSTRANSIENT    TUNSTRANSIENT
#endif
#else
#define TUNTAP_DEV "/dev/net/tun"
#endif

#include <jni.h>
#include "tuntap.h"

void setFdDev(JNIEnv *env, jobject this, int fd, char* dev) {
    jfieldID jfd, jdev; 
    jclass jclass; 
    jstring jstr;
    
    jclass = (*env)->GetObjectClass(env, this); 
    
    jfd = (*env)->GetFieldID(env, jclass, "fd", "I"); 
    (*env)->SetIntField(env, this, jfd , fd);
    
    jstr = (*env)->NewStringUTF(env, dev);
    jdev = (*env)->GetFieldID(env, jclass, "dev", "Ljava/lang/String;"); 
    (*env)->SetObjectField(env, this, jdev , jstr);
}

int getFd(JNIEnv *env, jobject this) {
    jfieldID jfd; 
    jclass jclass; 

    jclass = (*env)->GetObjectClass(env, this); 
    
    jfd = (*env)->GetFieldID(env, jclass, "fd", "I"); 
    return (*env)->GetIntField(env, this, jfd);
}

JFUNC(jint, openTun) {
    struct ifreq ifr;
    int fd;
    
#ifdef __FreeBSD__
    int i;
    size_t len = sizeof(i);
    
    sysctlbyname("net.link.tap.devfs_cloning", &i, &len, NULL, 0);
    if (!i) {
        printf("error: net.link.tap.devfs_cloning = 0\n");
        return 1;
    }
#endif
    
    if ((fd = open(TUNTAP_DEV, O_RDWR)) < 0) {
        printf("error: open\n");
        return 1;
    }
    
#ifdef __FreeBSD__
#ifdef USE_TRANSIENT
    i = 1;
    if (ioctl(fd, TAPSTRANSIENT, &i) < 0) {
        close(fd);
        printf("error: ioctl(TAPSTRANSIENT)\n");
        return 1;
    }
#endif

    if (ioctl(fd, TAPGIFNAME, &ifr) < 0) {
        close(fd);
        printf("error: ioctl(TAPGIFNAME)\n");
        return 1;
    }
#else
    memset(&ifr, 0, sizeof(ifr));
    
    ifr.ifr_flags = IFF_TAP | IFF_NO_PI;
    
    if (ioctl(fd, TUNSETIFF, &ifr) < 0) {
        close(fd);
        printf("error: ioctl(TUNSETIFF)\n");
        return 1;
    }
#endif
    
    setFdDev(env, this, fd, ifr.ifr_name);
    
    return 0;
}

JFUNC(void, close) {
    close(getFd(env, this));
}

JFUNC(void, write, jbyteArray jb, jint len) {
    int fd;
    jbyte *b;
    
    fd = getFd(env, this);
    b = (*env)->GetByteArrayElements(env, jb, NULL);
    
    #pragma GCC diagnostic ignored "-Wunused-result" 
    write(fd, b, len);
    
    (*env)->ReleaseByteArrayElements(env, jb, b, JNI_ABORT);
}

JFUNC(jint, read, jbyteArray jb) {
    int fd;
    jbyte *b;
    int len;
    
    fd = getFd(env, this);
    b = (*env)->GetByteArrayElements(env, jb, NULL);
    
    len = read(fd, b, (*env)->GetArrayLength(env, jb));
    
    (*env)->ReleaseByteArrayElements(env, jb, b, 0);
    return len;
}
