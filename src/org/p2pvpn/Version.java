/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn;

import java.io.IOException;
import java.io.InputStream;

/**
 * Application version
 * @author Monsterovich
 */
public class Version {
    private static final String VERSION = "0.12.1";

    /**
     * Get the hash of the current commit from git.
     * @return returns 7 byte hash string
     */
    private static String getGitRevision() {
        byte[] gitHashBytes = new byte[7];
        String gitHashString = null;
        InputStream gitHashStream = Version.class.getClassLoader().getResourceAsStream(".gitrevision");
        try {
            gitHashStream.read(gitHashBytes);
            gitHashString = new String(gitHashBytes);
            gitHashStream.close();
        } catch (IOException e) {
        }

        if (gitHashString == null) {
            gitHashString = "unknown";
        }
        
        return gitHashString;
    }
    
    /**
     * Return the application version, including the git hash.
     * @return application version string
     */
    public static String getVersion() {
        return String.format("%s (%s)", VERSION, getGitRevision());
    }
}
