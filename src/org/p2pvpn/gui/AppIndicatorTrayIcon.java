/*
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.gui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * The custom implementation of TrayIcon
 * @author Monsterovich
 */
public final class AppIndicatorTrayIcon extends TrayIcon {
    
    static String TRAYICON_TMP_FILE = "Lanemu_Tray.png";
    private String icon;
    
    static {
        String libname = null;
        try {
            switch (System.getProperty("os.arch")) {
                case "x86":
                case "i386":
                case "i486":
                case "i586":
                case "i686":
                    libname = "clib/libAppIndicator.so";
                    break;
                case "x86_64":
                case "amd64":
                    libname = "clib/libAppIndicator64.so";
                    break;
                default:
                    Logger.getLogger("").log(Level.WARNING, "Unknown os.arch. Still trying to load libAppIndicatorUnknown.so");
                    libname = "clib/libAppIndicatorUnknown.so";
                    break;
            }
            String clibPath = "";
            if (System.getenv().containsKey("CLIB_PATH")) {
                clibPath = System.getenv("CLIB_PATH");
            }
            File file = new File(Paths.get(clibPath, libname).toUri());
            if (file.exists()) {
                System.load(file.getCanonicalPath());
            } else {
                throw new Exception(String.format("loadLib failed. Library file %s is not found!", libname));
            }
            System.load(file.getCanonicalPath());
        } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, "Could not load App indicator library", e);
        }
    }
    
    public AppIndicatorTrayIcon(final Image image, final String tooltip, final PopupMenu popup) {
        super(image, tooltip, popup);

        // library status check
        try {
            test();
        } catch (UnsatisfiedLinkError e) {
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                // all natives must run in the same thread.
                for (int i = 0; i < popup.getItemCount(); i++) {
                    addMenu(popup.getItem(i));
                }

                // extract the icon from the resources, as we have to load it from the file into the tray icon library
                try {
                    File output = new File(TRAYICON_TMP_FILE);

                    if (!output.exists()) {
                        try {
                            BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 
                                BufferedImage.TYPE_INT_ARGB);

                            Graphics2D graphics = bufferedImage.createGraphics();
                            graphics.drawImage(image, 0, 0, null);
                            graphics.dispose();

                            ImageIO.write(bufferedImage, "png", output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    icon = output.getCanonicalPath();

                    trayRun(tooltip);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    
    public void clickCallback(MenuItem cbItem) {
        final PopupMenu popupMenu = this.getPopupMenu();
        for (int i = 0; i < popupMenu.getItemCount(); i++) {
            MenuItem item = popupMenu.getItem(i);
            if (item.equals(cbItem)) {
                for (ActionListener listener : item.getActionListeners()) {
                    listener.actionPerformed(null);
                }
                break;
            }
        }

    }
    
    @Override
    public void displayMessage(String caption, String text, MessageType messageType) {
        try {
            showMessage(caption, text);
        } catch (UnsatisfiedLinkError e) {
            super.displayMessage(caption, text, messageType);
        }
    }

    public native void test();

    private native void trayRun(String tooltip);
    private native void addMenu(MenuItem item);
    private native void showMessage(String caption, String text);
}
