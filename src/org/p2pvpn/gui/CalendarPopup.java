/*
    Copyright 2023-2024 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.p2pvpn.gui;
import com.github.lgooddatepicker.components.CalendarPanel;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.DatePickerSettings.DateArea;
import com.github.lgooddatepicker.zinternaltools.CustomPopup;
import com.github.lgooddatepicker.zinternaltools.CalendarSelectionEvent;
import com.github.lgooddatepicker.zinternaltools.YearMonthChangeEvent;
import com.github.lgooddatepicker.optionalusertools.CalendarListener;
import java.awt.Component;
import java.awt.Point;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JSpinner;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Calendar widget for the date entry field in InviteWindow. 
 * Uses internal library methods.
 * @author monsterovich
 */
public class CalendarPopup implements CalendarListener {
    private final CalendarPanel calendarPanel;
    private CustomPopup calendarPopup;
    private final Component parentComponent;
    private final JSpinner inputField;
    
    public CalendarPopup(Component component, JSpinner field) {
        parentComponent = component;
        inputField = field;

        calendarPanel = new CalendarPanel();
        DatePickerSettings settings = new DatePickerSettings();
        applyTheme(settings);
        calendarPanel.setSettings(settings);
        calendarPanel.addCalendarListener(this);
    }
    
    public void show() {
        Date originalDate = (Date) inputField.getModel().getValue();
        calendarPanel.setSelectedDate(originalDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        calendarPopup =
            new CustomPopup(
                calendarPanel,
                SwingUtilities.getWindowAncestor(parentComponent),
                null,
                BorderFactory.createLineBorder(UIManager.getColor("Table.selectionBackground")));

        Point pointOnScreen = new Point(); 
        SwingUtilities.convertPointToScreen(pointOnScreen, parentComponent);

        calendarPopup.setLocation(pointOnScreen.x, pointOnScreen.y + parentComponent.getHeight() + 1);
        calendarPopup.show();
        calendarPanel.requestFocus();
    }
    
    // fixme: remove this function when this is fixed
    // https://github.com/LGoodDatePicker/LGoodDatePicker/issues/195
    private void applyTheme(DatePickerSettings settings) {
        settings.setColor(DateArea.CalendarBackgroundNormalDates, UIManager.getColor("Panel.background"));
        settings.setColor(DateArea.BackgroundOverallCalendarPanel, UIManager.getColor("Panel.background"));
        settings.setColor(DateArea.TextFieldBackgroundValidDate, UIManager.getColor("TextField.background"));
        settings.setColor(DateArea.CalendarBackgroundSelectedDate, UIManager.getColor("Table.selectionBackground"));
        settings.setColor(DateArea.BackgroundTopLeftLabelAboveWeekNumbers, UIManager.getColor("Label.background"));
        settings.setColor(DateArea.BackgroundMonthAndYearMenuLabels, UIManager.getColor("Label.background"));
        settings.setColor(DateArea.BackgroundTodayLabel, UIManager.getColor("Label.background"));
        settings.setColor(DateArea.BackgroundClearLabel, UIManager.getColor("Button.background"));
        settings.setColor(DateArea.CalendarTextNormalDates, UIManager.getColor("Label.foreground"));
        settings.setColor(DateArea.CalendarBorderSelectedDate, UIManager.getColor("Table.selectionBackground").darker());
        settings.setColor(DateArea.BackgroundCalendarPanelLabelsOnHover, UIManager.getColor("ComboBox.buttonHighlight"));
        settings.setColor(DateArea.CalendarTextWeekdays, UIManager.getColor("Button.foreground"));
        settings.setColorBackgroundWeekdayLabels(UIManager.getColor("Button.background"), true);
    }

    @Override
    public void selectedDateChanged(CalendarSelectionEvent event) {
        LocalDate calendarDate = event.getSource().getSelectedDate();
        if (calendarDate != null && event.isDuplicate()) {
            LocalDateTime originalDateTime = ((Date) inputField.getModel().getValue()).toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime newDateTime = calendarDate.atTime(originalDateTime.getHour(), originalDateTime.getMinute());
            inputField.getModel().setValue(Date.from(newDateTime.atZone(ZoneId.systemDefault()).toInstant()));
            calendarPopup.hide();
        }
    }
    
    @Override
    public void yearMonthChanged(YearMonthChangeEvent event) {
        
    }
}
