/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network;

import org.p2pvpn.network.bittorrent.BitTorrentTracker;
import org.p2pvpn.network.bittorrent.DHT;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;
import org.p2pvpn.network.bandwidth.TokenBucket;
import org.p2pvpn.tools.AdvProperties;
import org.p2pvpn.tools.CryptoUtils;
import org.p2pvpn.tools.SocketAddrStr;

/**
 * The ConnectionManager is the central point of the Lanemu network. It
 * coordinates the different layers of the network.
 *
 * @author Wolfgang Ginolas
 */
public class ConnectionManager implements Runnable {

    final static private String WHATISMYIPV4_URL = "http://icanhazip.com";
    final static private String WHATISMYIPV6_URL = "http://ipv6.icanhazip.com";
    final static private long WHATISMYIP_REFRESH_S = 10 * 60;
    final static private int SOCKET_TIMEOUT_MS = 10000;

    final static private double SEND_BUCKET_SIZE = 10 * 1024;

    private ServerSocket server;                                // the ServerSocked to accept connections
    private int serverPort;                                     // the local port
    private final boolean udp;                                  // use UDP or TCP for traffic?
    private final boolean upnp;                                 // upnp enabled?
    private final String macBlacklist;                          // blacklist of mac addresses to not send to other peers
    private final Vector<String[]> peerBlacklist;               // blacklist of peers (parsed)
    private final PeerID localAddr;                             // the local PeerID
    private final ScheduledExecutorService scheduledExecutor;   // a scheduled exicutor used for various tasks
    private final Router router;                                // the Router
    private final Connector connector;                          // the Connector
    private final UPnPPortForward uPnPPortForward;              // uPnP port forwaring object
    private final Vector<BitTorrentTracker> bitTorrentTrackers; // the BitTorrentTracker list
    private DatagramSocket dsocket;                             // a socket for UDP traffic
    private DHT dht;                                            // Bit Torrent DHT

    private String whatIsMyIPv4, whatIsMyIPv6;                  // the local IP returned by whatismyip site

    private final AdvProperties accessCfg;                      // the access invitation
    private byte[] networkKey;                                  // network key used for encryption

    private final TokenBucket sendLimit, recLimit;              // maximum bandwidth
    private final Pinger pinger;                                // the Pinger

    private int sendBufferSize;                                 // the send buffer size
    private boolean tcpFlush;                                   // flush after each packet send?
    
    private boolean udpThreadRunning;                           // is UDP thread running?

    /**
     * Create a new ConnectionManager
     *
     * @param accessCfg the access invitation
     * @param serverPort the local server port
     * @param upnp UPnP enabled?
     * @param udp use UDP or TCP for traffic?
     * @param macBlacklist blacklist of mac addresses
     */
    public ConnectionManager(AdvProperties accessCfg, int serverPort, boolean upnp, boolean udp, String macBlacklist) {
        sendBufferSize = TCPConnection.DEFAULT_MAX_QUEUE;
        tcpFlush = TCPConnection.DEFAULT_TCP_FLUSH;
        udpThreadRunning = false;
        peerBlacklist = new Vector<>();

        this.serverPort = serverPort;
        this.accessCfg = accessCfg;
        this.upnp = upnp;
        this.udp = udp;
        this.macBlacklist = macBlacklist;

        scheduledExecutor = Executors.newScheduledThreadPool(10);
        localAddr = new PeerID(accessCfg.getPropertyBytes("access.publicKey", null), true);
        router = new Router(this);
        connector = new Connector(this);
        bitTorrentTrackers = new Vector<>();
        dht = null;
        uPnPPortForward = new UPnPPortForward(this);
        whatIsMyIPv4 = null;
        whatIsMyIPv6 = null;

        sendLimit = new TokenBucket(0, SEND_BUCKET_SIZE);
        recLimit = new TokenBucket(0, SEND_BUCKET_SIZE);
        pinger = new Pinger(this);

        calcNetworkKey();
        
        (new Thread(this, "ConnectionManager")).start();
        
        scheduledExecutor.schedule(this::checkWhatIsMyIP, 1, TimeUnit.SECONDS);
    }

    /**
     * Calculate the network key used for encryption
     */
    private void calcNetworkKey() {
        byte[] b = accessCfg.getPropertyBytes("network.signature", null);
        MessageDigest md = CryptoUtils.getMessageDigest();
        md.update("secretKey".getBytes());	// make sure the key differs from
        // other hashes created from the publicKey
        networkKey = md.digest(b);
    }

    /**
     * Find out the IPs of the local network adapters.
     */
    public void updateLocalIPs() {
        String ipList = "";
        String ipv6List = "";

        String[] macAddressesStrings = macBlacklist.split("\n");

        try {
            Enumeration<NetworkInterface> is = NetworkInterface.getNetworkInterfaces();
            while (is.hasMoreElements()) {
                NetworkInterface i = is.nextElement();
                
                if (i.getHardwareAddress() == null) {
                    continue;
                }

                boolean existsInBlacklist = false;

                MacAddress mac = new MacAddress(i.getHardwareAddress());
                for (String macAddressString : macAddressesStrings) {
                    if (new MacAddress(macAddressString).equals(mac)) {
                        existsInBlacklist = true;
                        break;
                    }
                }

                if (existsInBlacklist) {
                    continue;
                }

                if (!i.getName().toLowerCase().startsWith("tap")
                    && !i.getDisplayName().toLowerCase().startsWith("tap")
                    && !i.getName().equals("lo")) {

                    Enumeration<InetAddress> as = i.getInetAddresses();
                    while (as.hasMoreElements()) {
                        InetAddress a = as.nextElement();
                        if (a instanceof Inet4Address) {
                            String s = a.getHostAddress();
                            ipList += " " + s;
                        }
                        if (a instanceof Inet6Address) {
                            String s = a.getHostAddress();
                            ipv6List += " " + s;
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Logger.getLogger("").log(Level.WARNING, "", ex);
        }
        if (whatIsMyIPv4 != null) {
            ipList += " " + whatIsMyIPv4;
        }
        if (whatIsMyIPv6 != null) {
            ipv6List += " " + whatIsMyIPv6;
        }
        
        router.setLocalPeerInfo("local.port", String.valueOf(serverPort));
        router.setLocalPeerInfo("local.ips", ipList.substring(1));
        router.setLocalPeerInfo("local.ip6s", ipv6List.substring(1));
    }

    /**
     * Periodically check the local IPs.
     */
    private void checkWhatIsMyIP() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                new URL(WHATISMYIPV4_URL).openConnection().getInputStream()));
            InetAddress address = InetAddress.getByName(in.readLine());
            if (address instanceof Inet4Address) {
                whatIsMyIPv4 = address.getHostAddress();
            }
        } catch (IOException ex) {
            Logger.getLogger("").log(Level.WARNING, "can not determine external IPv4 address", ex);
        }
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                new URL(WHATISMYIPV6_URL).openConnection().getInputStream()));
            InetAddress address = InetAddress.getByName(in.readLine());
            if (address instanceof Inet6Address) {
                whatIsMyIPv6 = address.getHostAddress();
            }
        } catch (IOException ex) {
        }
        updateLocalIPs();
        scheduledExecutor.schedule(this::checkWhatIsMyIP, WHATISMYIP_REFRESH_S, TimeUnit.SECONDS);
    }

    public PeerID getLocalAddr() {
        return localAddr;
    }

    public AdvProperties getAccessCfg() {
        return accessCfg;
    }

    public ScheduledExecutorService getScheduledExecutor() {
        return scheduledExecutor;
    }

    /**
     * Called, when a TCPConnection is established.
     *
     * @param connection the connection
     */
    public void newConnection(TCPConnection connection) {
        new P2PConnection(this, connection);
    }

    /**
     * Called, when a new P2PConnectrion is established.
     *
     * @param p2pConnection
     */
    public void newP2PConnection(P2PConnection p2pConnection) {
        router.newP2PConnection(p2pConnection);
        handleBlacklist(new PeerID[] { p2pConnection.getRemoteAddr() });
    }

    /**
     * Process (ban/unban) all members that are present in the list.
     * If null is specified, the list of all peers is taken.
     *
     * @param peers the peer list
     */
    public void handleBlacklist(PeerID[] peers) {
        if (peerBlacklist.isEmpty()) {
            return;
        }

        if (peers == null) {
            peers = router.getPeers();
        }

        Vector<PeerID> badPeers = new Vector<>();
        Vector<PeerID> goodPeers = new Vector<>();
        
        for (String[] banLine : peerBlacklist) {
            String banAddress = banLine[0];
            String banPeerID = banLine[1];

            for (PeerID peer : peers) {
                if (getLocalAddr().equals(peer)) {
                    continue;
                }
                
                String vpnIP = router.getPeerInfo(peer, "vpn.ip");

                boolean exactPeer = peer.toString().equals(banPeerID);
                if (router.isConnectedTo(peer)) {
                    InetAddress address = router.getConnection(peer).getConnection().getAddress().getAddress();
                    String hostAddress = address.getHostAddress();
                    String hostName = address.getHostName();
                    boolean isBanned = exactPeer || 
                        hostAddress.equals(banAddress) || 
                        hostName.equals(banAddress) ||
                        (vpnIP != null && vpnIP.equals(banAddress));

                    if (isBanned) {
                        badPeers.add(peer);

                        Logger.getLogger("").log(Level.INFO, 
                            String.format("Disconnecting a banned peer %s %s (%s) (direct connecion).", 
                                peer.toString(),
                                hostAddress,
                                hostName));
                    }
                } else if (exactPeer || (vpnIP != null && vpnIP.equals(banAddress))) {
                    badPeers.add(peer);
                    Logger.getLogger("").log(Level.INFO, 
                     String.format("Removing a banned peer %s %s from the list of peers (indirect connection).", 
                                peer.toString(),
                                vpnIP));
                } else {
                    goodPeers.add(peer);
                }
            }

            for (PeerID peer : badPeers) {
                if (router.isConnectedTo(peer)) {
                    router.getConnection(peer).close();
                }

                router.banPeer(peer);
            }

            for (PeerID peer : goodPeers) {
                router.removeBlockedMAC(peer);
            }
        }
    }

    /**
     * A thread to accept connections from other peers.
     */
    @Override
    public void run() {
        Thread udpListeningThread = null;

        try {
            if (upnp) {
                uPnPPortForward.open();
            }
            
            server = new ServerSocket(serverPort);
            serverPort = server.getLocalPort();
            
            try {
                if (udp) {
                    dsocket = new DatagramSocket(null);
                    dsocket.setReuseAddress(true);
                    dsocket.bind(new InetSocketAddress(serverPort));

                    router.setLocalPeerInfo("local.port.udp", 
                        String.valueOf(serverPort));
                    
                    udpThreadRunning = true;
                    udpListeningThread = new Thread(this::udpListeningThread, "ConnectionManager.udpListeningThread");
                    udpListeningThread.start();
                }
            } catch (SocketException e) {
                Logger.getLogger("").log(Level.SEVERE, String.format("Couldn't bind on UDP port %d", serverPort), e);
            }

            Logger.getLogger("").log(Level.INFO, "Listening on port {0}", String.valueOf(serverPort));

            while (true) {
                Socket socket = server.accept();
                new TCPConnection(this, socket, networkKey);
            }
        } catch (IOException e) {
            Logger.getLogger("").log(Level.SEVERE, String.format("Not listening on %d anymore", serverPort), e);
        }
        
        if (udpListeningThread != null) {
            dsocket.close();
            udpThreadRunning = false;

            try {
                udpListeningThread.join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Get UDP socket from connection manager.
     * 
     * @return udp socket
     */
    public DatagramSocket getDatagramSocket() {
        return dsocket;
    }
    
    /**
     * A thread that handles incoming UDP packets.
     */
    public void udpListeningThread() {
        DatagramPacket dpacket = new DatagramPacket(new byte[TCPConnection.MAX_PACKET_SIZE], TCPConnection.MAX_PACKET_SIZE);

        while (udpThreadRunning) {
            try {
                dsocket.receive(dpacket);

                ByteArrayInputStream stream = new ByteArrayInputStream(dpacket.getData());

                byte[] peerBytes = new byte[PeerID.PEERID_LENGTH];
                stream.read(peerBytes);

                int high = stream.read();
                int low = stream.read();

                int size = (high << 8) + low;

                if (size > TCPConnection.MAX_PACKET_SIZE) {
                    throw new IOException("Packet too large");
                }

                if (size < 0) {
                    throw new IOException("Invalid packet size");
                }

                PeerID senderPeer = new PeerID(peerBytes, false);
                P2PConnection connection = router.getConnection(senderPeer);
                
                if (connection != null) {
                    byte[] packet = new byte[size];
                    stream.read(packet);
                    
                    connection.getConnection().processUDPPacket(packet, dpacket.getPort());
                }
            } catch (IOException e) { 
            }
        }
    }
    
    /**
     * Add the IPs stored in the access invitation to the known hosts list.
     *
     * @param accessCfg the access invitation
     */
    public void addIPs(AdvProperties accessCfg) {
        connector.addIPs(accessCfg);

        bitTorrentTrackers.clear();

        int i = 0;
        while (accessCfg.containsKey("network.bootstrap.tracker." + i)) {
            bitTorrentTrackers.add(new BitTorrentTracker(this, accessCfg.getProperty("network.bootstrap.tracker." + i)));
            i++;
        }

        if (dht != null) {
            dht.stopDHTReceivingThread();
        }

        String dhtProperty = accessCfg.getProperty("network.bootstrap.DHT");
        if (dhtProperty != null) {
            try {
                dht = new DHT(this);
            } catch (Exception ex) {
                Logger.getLogger("").log(Level.WARNING, "", ex);
            }
        } else {
            dht = null;
        }
    }

    /**
     * Try to connect to the given host.
     *
     * @param host the host
     * @param port the port
     */
    public void connectTo(String host, int port) {
        try {
            connectTo(InetAddress.getByName(host), port);
        } catch (UnknownHostException ex) {
            Logger.getLogger("").log(Level.WARNING, "", ex);
        }
    }

    /**
     * Try to connect to the given host.
     *
     * @param host the host
     * @param port the port
     */
    public void connectTo(InetAddress host, int port) {
        new ConnectTask(host, port);
    }

    /**
     * Try to connect to the given host.
     *
     * @param addr the host and port using the format "hist:port"
     * @return returns true if address if valid
     */
    public boolean connectTo(String addr) {
        try {
            InetSocketAddress a = SocketAddrStr.parseSocketAddr(addr);
            connectTo(a.getAddress(), a.getPort());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Stop this network and close all connections
     */
    public void close() {
        try {
            scheduledExecutor.shutdownNow();
            router.close();
            if (server != null) {
                server.close();
            }
            if (uPnPPortForward.getInitialized()) {
                uPnPPortForward.close();
            }
        } catch (IOException e) {
            Logger.getLogger("").log(Level.WARNING, "", e);
        }
    }

    /**
     * A Task which tries to connect another peer
     */
    private class ConnectTask implements Runnable {

        private final InetAddress host;
        private final int port;

        /**
         * Try to connect another peer.
         *
         * @param host the host
         * @param port the port
         */
        public ConnectTask(InetAddress host, int port) {
            this.host = host;
            this.port = port;
            (new Thread(this, "ConnectTask")).start();
        }

        @Override
        public void run() {
            Socket socket;

            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(host, port), SOCKET_TIMEOUT_MS);
                new TCPConnection(ConnectionManager.this, socket, networkKey);

            } catch (IOException e) {
                connector.removeObsoleteIP(host, port, "stored");
            }
        }
    }

    public int getServerPort() {
        return serverPort;
    }

    public Router getRouter() {
        return router;
    }

    public Connector getConnector() {
        return connector;
    }

    public UPnPPortForward getUPnPPortForward() {
        return uPnPPortForward;
    }
    
    public boolean isUDP() {
        return udp;
    }

    public TokenBucket getSendLimit() {
        return sendLimit;
    }

    public TokenBucket getRecLimit() {
        return recLimit;
    }

    public int getSendBufferSize() {
        return sendBufferSize;
    }

    public void setSendBufferSize(int sendBufferSize) {
        this.sendBufferSize = sendBufferSize;
    }

    public boolean isTCPFlush() {
        return tcpFlush;
    }

    public void setTCPFlush(boolean tcpFlush) {
        this.tcpFlush = tcpFlush;
    }

    public void setPeerBlacklist(String peerBlacklist) {
        if (!this.peerBlacklist.isEmpty()) {
            this.peerBlacklist.clear();
        }

        if (peerBlacklist == null) {
            return;
        }
        
        String[] bannedPeers = peerBlacklist.split("\n");

        for (String bannedPeer : bannedPeers) {
            if (bannedPeer.startsWith("#")) {
                continue;
            }
            
            String[] banParts = bannedPeer.split(" ");
            if (banParts.length == 0) {
                continue;
            }
            
            String[] banLine = {
                banParts[0] != null ? banParts[0] : bannedPeer,
                banParts.length > 1 ? banParts[1] : ""
            };

            this.peerBlacklist.add(banLine);
        }
    }
}
