/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network;

import org.p2pvpn.network.bandwidth.MeasureBandwidth;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;
import javax.crypto.SecretKey;
import org.p2pvpn.tools.CryptoUtils;

/**
 * This is the lowest layer in the Lanemu network. It encrypts packets and
 * sends/receives them using TCP.
 *
 * @author Wolfgang Ginolas
 */
public class TCPConnection implements Runnable {

    private static final double BUCKET_TIME = 0.5;
    private static final int BUCKET_LEN = 10;

    public static final int DEFAULT_MAX_QUEUE = 100;
    public static final boolean DEFAULT_TCP_FLUSH = false;

    public static final int MAX_PACKET_SIZE = 10 * 1024;
    
    private static final int BITTORRENT_PROTOCOL_STRING_SIZE = 19;

    private MeasureBandwidth bwIn, bwOut;       // the currently used bandwidth
    
    private Cipher cIn, cOut;                   // the ciphers for sending and receiving

    private SecretKey key;                      // the current encryption key

    private ConnectionManager connectionManager;// the ConnectionManager
    private Socket socket;                      // the socket for this connection
    private InputStream in;                     // InputStream for this connection
    private BufferedOutputStream out;           // OutputStream for this connection
    private SocketAddress peer;                 // the remote address
    private P2PConnection listener;             // the upper network layer

    private final Queue<Packet> sendingQueue;   // the sending queue
    private boolean closed;                     // is this connection closed?

    private long lastActive;                    // time of the last received packet

    private int remoteUDPPort;                  // port of this connection received from UDP packet
    private final Queue<byte[]> udpRecvQueue;   // queue for processing UDP packets for this connection

    /**
     * Create a new TCPConnection
     *
     * @param connectionManager the ConnectionManager
     * @param socket the Socket of the connection
     * @param keyBytes the encryption kay to use
     */
    public TCPConnection(ConnectionManager connectionManager, Socket socket, byte[] keyBytes) {
        this.connectionManager = connectionManager;
        this.socket = socket;
        peer = socket.getRemoteSocketAddress();
        sendingQueue = new LinkedList<>();
        closed = false;
        bwIn = new MeasureBandwidth(BUCKET_TIME, BUCKET_LEN);
        bwOut = new MeasureBandwidth(BUCKET_TIME, BUCKET_LEN);
        cIn = CryptoUtils.getSymmetricCipher();
        cOut = CryptoUtils.getSymmetricCipher();
        lastActive = System.currentTimeMillis();
        remoteUDPPort = 0;
        udpRecvQueue = new LinkedList<>();

        try {
            in = socket.getInputStream();
            out = new BufferedOutputStream(socket.getOutputStream());
            changeKey(keyBytes);
            this.connectionManager.newConnection(this);
            (new Thread(this, "TCPConnection " + peer)).start();
            (new Thread(this::sendingThread, "TCPConnection.sendingThread " + peer)).start();
            if (this.connectionManager.isUDP()) {
                (new Thread(this::udpProcessingThread, "TCPConnection.udpProcessingThread " + peer)).start();
            }
        } catch (IOException e) {
            Logger.getLogger("").log(Level.WARNING, "", e);
        }
    }

    /**
     * Change the encryption key.
     *
     * @param keyBytes the new key
     */
    public final void changeKey(byte[] keyBytes) {
        key = CryptoUtils.decodeSymmetricKey(keyBytes);
    }

    /**
     * Read a byte integer from the connection.
     *
     * @return the int
     * @throws java.io.IOException
     */
    private int readByte() throws IOException {
        int b;
        b = in.read();
        if (b == -1) {
            throw new IOException("Connection to " + peer + " lost");
        }
        return b;
    }

    /**
     * Receive packets.
     */
    @Override
    public void run() {
        boolean silent = false;

        byte[] buffer = new byte[MAX_PACKET_SIZE];

        try {
            while (true) {
                int high = readByte();
                int low = readByte();

                int size = (high << 8) + low;

                if (size > MAX_PACKET_SIZE) {
                    throw new IOException("Packet too large");
                }

                int rest = size;
                int off = 0;

                if (high == BITTORRENT_PROTOCOL_STRING_SIZE && low == 'B') {
                    byte[] ignoreBTTestBuffer = new byte[BITTORRENT_PROTOCOL_STRING_SIZE - 1];
                    int len = 0;
                    for (int i = 0; i < ignoreBTTestBuffer.length; i += len) {
                        len = in.read(ignoreBTTestBuffer, i, ignoreBTTestBuffer.length - i);
                        if (len == -1) {
                            throw new IOException("Connection to " + peer + " lost");
                        }
                    }
                    if (new String(ignoreBTTestBuffer).contentEquals("itTorrent protocol")) {
                        silent = true;
                        break;
                    }
                    rest -= ignoreBTTestBuffer.length;
                    off += ignoreBTTestBuffer.length;
                    System.arraycopy(ignoreBTTestBuffer, 0, buffer, 0, ignoreBTTestBuffer.length);
                }

                while (rest > 0) {
                    int len = in.read(buffer, off, rest);
                    if (len == -1) {
                        throw new IOException("Connection to " + peer + " lost");
                    }
                    rest -= len;
                    off += len;
                }

                byte[] packet = new byte[size];
                System.arraycopy(buffer, 0, packet, 0, size);
                handleEncryptedPacket(packet, false);
            }
        } catch (IOException e) {
        }

        if (listener != null) {
            listener.connectionClosed(silent);
        }
        closed = true;

        synchronized (sendingQueue) {
            sendingQueue.notify();
        }
        
        if (connectionManager.isUDP()) {
            synchronized (udpRecvQueue) {
                udpRecvQueue.notify();
            }
        }

        try {
            socket.close();
        } catch (IOException e) {
            Logger.getLogger("").log(Level.WARNING, "", e);
        }
    }
    
    /**
     * Get packets from the queue and send them.
     */
    private void sendingThread() {
        try {
            while (true) {
                if (closed) {
                    break;
                }
                Packet packet;
                synchronized (sendingQueue) {
                    packet = sendingQueue.poll();
                }
                if (packet == null) {
                    out.flush();
                    try {
                        synchronized (sendingQueue) {
                            sendingQueue.wait();
                        }
                    } catch (InterruptedException ex) {
                    }
                } else {
                    sendEncypted(packet, false);
                    if (!packet.isUDP() && connectionManager.isTCPFlush()) {
                        out.flush();
                    }
                }
            }
        } catch (IOException iOException) {
            close();
        }
    }
    
    /**
     * Process incoming encrypted packets from UDP socket.
     */
    private void udpProcessingThread() {
        while (true) {
            if (closed) {
                break;
            }
            byte[] packet;
            synchronized (udpRecvQueue) {
                packet = udpRecvQueue.poll();
            }
            if (packet == null) {
                try {
                    synchronized (udpRecvQueue) {
                        udpRecvQueue.wait();
                    }
                } catch (InterruptedException ex) {
                }
            } else {
                handleEncryptedPacket(packet, true);
            }
        }
    }
    
    /**
     * Handle UDP packets in separate threads or in the receiving thread.
     *
     * @param packet UDP packet
     * @param udpPort port from which the packet was sent
     * 
     */
    public void processUDPPacket(byte[] packet, int udpPort) {
        if (remoteUDPPort == 0) {
            if (handleEncryptedPacket(packet, true)) {
                remoteUDPPort = udpPort;
            }
        } else {
            synchronized (udpRecvQueue) {
                udpRecvQueue.offer(packet);
                udpRecvQueue.notify();
            }
        }
    }

    /**
     * Encrypt a packet and send it.
     *
     * @param packet the packet
     * @param flush flush the stream?
     */
    private void sendEncypted(Packet packet, boolean flush) {
        byte[] nonce = CryptoUtils.getNonce();

        try {
            cOut.init(Cipher.ENCRYPT_MODE, key, CryptoUtils.getAlgorithmParamSpec(nonce));
        } catch (InvalidKeyException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger("").log(Level.SEVERE, null, ex);
            close();
            return;
        }
        
        try {
            byte[] encryptedData = cOut.doFinal(packet.getData());

            sendToSocket(nonce, encryptedData, packet.isUDP(), flush);
        } catch (BadPaddingException | IllegalBlockSizeException t) {
            Logger.getLogger("").log(Level.SEVERE, null, t);
            close();
        }
    }

    /**
     * Send an encrypted packet through the socket.
     *
     * @param nonce packet "Number Once"
     * @param encryptedData encrypted data in the packet
     * @param udp use UDP or TCP?
     * @param flush flush the stream?
     */
    private void sendToSocket(byte[] nonce, byte[] encryptedData, boolean udp, boolean flush) {
        try {
            int packetLen = nonce.length + encryptedData.length;

            int high = (packetLen & 0xFF00) >> 8;
            int low = packetLen & 0xFF;

            if (udp) {
                DatagramSocket dsocket = connectionManager.getDatagramSocket();
                int port = 0;
                
                if (dsocket != null && (remoteUDPPort > 0 || (port = findUDPPort()) > 0)) {
                    connectionManager.getSendLimit().waitForTokens(2 + PeerID.PEERID_LENGTH + packetLen);
                    
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();

                    stream.write(connectionManager.getLocalAddr().getId());
                    stream.write(high);
                    stream.write(low);
                    stream.write(nonce);
                    stream.write(encryptedData);

                    byte[] dataBytes = stream.toByteArray();
                    
                    InetSocketAddress address = (InetSocketAddress)socket.getRemoteSocketAddress();
                    DatagramPacket packet = new DatagramPacket(dataBytes, dataBytes.length, 
                        address.getAddress(), remoteUDPPort > 0 ? remoteUDPPort : port);
                    
                    dsocket.send(packet);

                    bwOut.countPacket(2 + PeerID.PEERID_LENGTH + packetLen);
                    return;
                }
            }
            
            connectionManager.getSendLimit().waitForTokens(2 + packetLen);
            
            out.write(high);
            out.write(low);
            out.write(nonce);
            out.write(encryptedData);
            if (flush) {
                out.flush();
            }

            bwOut.countPacket(2 + packetLen);
        } catch (IOException iOException) {
            close();
        }
    }
    
    /**
     * Look up the UDP port in the peer data table of this connection. 
     * If the port is found, save it and use it further.
     *
     * @return UDP port (returns 0 if UDP is disabled for the connection)
     */
    private int findUDPPort() {
        if (listener != null) {
            String portStr = connectionManager.getRouter()
                .getPeerInfo(listener.getRemoteAddr(), "local.port.udp");
            if (portStr != null) {
                return Integer.parseInt(portStr);
            }
        }
        
        return 0;
    }

    /**
     * Handle an incoming encrypted packet.
     *
     * @param packet the packet
     * @param udp is packet a UDP packet?
     * @return true if packet was handled successfully
     */
    public synchronized boolean handleEncryptedPacket(byte[] packet, boolean udp) {
        lastActive = System.currentTimeMillis();
        
        int fullPacketLen = 2 + packet.length;
        
        if (udp) {
            fullPacketLen += PeerID.PEERID_LENGTH;
        }
        
        bwIn.countPacket(fullPacketLen);
        if (!connectionManager.getRecLimit().tokensAvailable(fullPacketLen)) {
            return false;		// drop packet to limit bandwidth
        }

        try {
            cIn.init(Cipher.DECRYPT_MODE, key, CryptoUtils.getAlgorithmParamSpec(packet));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException t) {
            if (!udp) {
                close();
            }
            return false;
        }
        
        try {
            if (listener != null) {
                byte[] decryptedPacket = cIn.doFinal(packet, 
                    CryptoUtils.GCM_NONCE_LENGTH, 
                    packet.length - CryptoUtils.GCM_NONCE_LENGTH);
                
                listener.receive(decryptedPacket);
            }
        } catch (BadPaddingException | IllegalBlockSizeException t) {
            if (!udp) {
                close();
            }
            return false;
        }
        
        return true;
    }

    /**
     * Set the object of the upper layer.
     *
     * @param listener the upper layer
     */
    public void setListener(P2PConnection listener) {
        this.listener = listener;
    }

    /**
     * Put a packet in the sending queue.
     *
     * @param packet the packet
     * @param highPriority a high priority packet? A high priority packer won't
     * be dropped even if the send queue is full.
     * @param udp is packet a udp packet?
     */
    public void send(byte[] packet, boolean highPriority, boolean udp) {
        synchronized (sendingQueue) {
            if (highPriority || sendingQueue.size() < connectionManager.getSendBufferSize()) {
                sendingQueue.offer(new Packet(packet, udp));
                sendingQueue.notify();
            }
        }
    }

    /**
     * Close the connection.
     */
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            Logger.getLogger("").log(Level.WARNING, "", e);
        }
    }

    @Override
    public String toString() {
        return peer.toString();
    }

    public InetSocketAddress getAddress() {
        return ((InetSocketAddress) peer);
    }

    public MeasureBandwidth getBwIn() {
        return bwIn;
    }

    public MeasureBandwidth getBwOut() {
        return bwOut;
    }

    public long getLastActive() {
        return lastActive;
    }
    
    public static class Packet {
        private final byte[] data;
        private final boolean udp;
        
        Packet(final byte[] data, final boolean udp) {
            this.data = data;
            this.udp = udp;
        }
        
        public byte[] getData() {
            return data;
        }
        
        public boolean isUDP() {
            return udp;
        }
    }
}
