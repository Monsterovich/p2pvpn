/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network.bittorrent;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.p2pvpn.network.ConnectionManager;
import org.p2pvpn.network.bittorrent.bencode.Bencode;
import org.p2pvpn.network.bittorrent.bencode.BencodeInt;
import org.p2pvpn.network.bittorrent.bencode.BencodeList;
import org.p2pvpn.network.bittorrent.bencode.BencodeMap;
import org.p2pvpn.network.bittorrent.bencode.BencodeObject;
import org.p2pvpn.network.bittorrent.bencode.BencodeString;
import org.p2pvpn.network.Connector.Endpoint;

// https://www.bittorrent.org/beps/bep_0005.html
public class DHT implements Runnable {

    private static final String[] DHT_BOOTSTRAP_NODES = {
        "router.bittorrent.com:6881",
        "dht.transmissionbt.com:6881",
        "dht.libtorrent.org:25401"
    };
    
    private ConnectionManager connectionManager;
    private final Thread receivingThread;
    private boolean receivingThreadRunning = true;

    private static final int PACKET_LEN = 10 * 1024;
    private static final BigInteger MASK = new BigInteger("1").shiftLeft(160);
    private static final int PEER_QUEUE_SIZE = 10;
    private static final int MAX_BAD = 4;
    private static final int MAX_GOOD = 4;

    private static final int MAX_DHT_PEER_SPAM_ATTEMPTS = 3;
    private static final int PEER_ANNOUNCE_INTERVAL_MS = 15 * 60 * 1000;
    private static final int ANNOUNCE_PEER_CACHE_MAX = 1024;
    private static final int SUSPICIOUS_PEERS_MAX = 32;


    private DatagramSocket dSock;

    private BencodeString id;
    private BencodeString searchID;
    private BigInteger searchIDInt;
    
    private final PriorityBlockingQueue<Contact> peerQueue;

    private final Map<Contact, Integer> peerBad;

    private final Map<Contact, Integer> maliciousPeerCounts;
    private final Map<InetSocketAddress, Contact> maliciousPeerAddresses;
    private final Vector<Contact> maliciousPeers;

    private Contact lastRemotePeer;
    private InetSocketAddress lastAddress;

    private final Map<Contact, Long> announcePeerLastTime;

    public DHT(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        
        byte[] peerId = new byte[Contact.CONTACT_ID_LEN];
        new Random().nextBytes(peerId);
        id = new BencodeString(peerId);

        searchID = new BencodeString(BitTorrent.networkHash(connectionManager.getAccessCfg().getPropertyBytes("network.signature", null), Contact.CONTACT_ID_LEN
        ));
        searchIDInt = unsigned(new BigInteger(searchID.getBytes()));

        try {
            dSock = new DatagramSocket();
        } catch (SocketException e) {
            Logger.getLogger("").log(Level.SEVERE, "Couldn't bind DHT port", e);
        }

        peerQueue = new PriorityBlockingQueue<>(PEER_QUEUE_SIZE, (Contact c1, Contact c2) -> {
            BigInteger dist1 = searchDist(c1);
            BigInteger dist2 = searchDist(c2);
            return dist1.compareTo(dist2);
        });

        peerBad = Collections.synchronizedMap(new HashMap<>());
        maliciousPeerCounts = new HashMap<>();
        maliciousPeers = new Vector<>();
        maliciousPeerAddresses = new HashMap<>();

        announcePeerLastTime = new HashMap<>();

        receivingThread = new Thread(this::receivingThread, "DHT.receivingThread");
        receivingThread.start();

        (new Thread(this::bootstrap, "DHT.bootstrap")).start();
  
        schedule(1);
    }

    /**
     * Schedule DHT network peer processing.
     *
     * @param seconds poll in seconds.
     */
    private void schedule(int seconds) {
        connectionManager.getScheduledExecutor().schedule(this, seconds, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        if (dSock != null) {
            processPeers();
            schedule(1);
        }
    }

    /**
     * Stops the DHT receiving thread, which is responsible for listening to incoming DHT packets.
     * This method sets the receivingThreadRunning flag to false, waits for the thread to finish,
     * and then closes the DatagramSocket used for receiving.
     */
    public void stopDHTReceivingThread() {
        dSock.close();
        receivingThreadRunning = false;

        try {
            receivingThread.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Calculates the unsigned XOR distance between the `searchIDInt` and the ID of a contact.
     *
     * @param c The contact for which the distance is calculated.
     * @return The unsigned XOR distance as a BigInteger.
     */
    private BigInteger searchDist(Contact c) {
        return unsigned(searchIDInt.xor(c.getId()));
    }

    /**
     * Converts a signed BigInteger to its unsigned equivalent.
     *
     * @param i The signed BigInteger to convert to unsigned.
     * @return The equivalent unsigned BigInteger.
     */
    public static BigInteger unsigned(BigInteger i) {
        if (i.signum() < 0) {
            return MASK.add(i);
        } else {
            return i;
        }
    }

    /**
     * Sends a ping request to a specified address in the DHT network.
     *
     * @param addr The address to which the ping request is sent.
     * @throws IOException If there is an I/O error while sending the ping request.
     */
    private void ping(SocketAddress addr) throws IOException {
        BencodeMap m = new BencodeMap();
        m.put(new BencodeString("t"), new BencodeString("ping"));
        m.put(new BencodeString("y"), new BencodeString("q"));
        m.put(new BencodeString("q"), new BencodeString("ping"));
        BencodeMap a = new BencodeMap();
        a.put(new BencodeString("id"), id);
        m.put(new BencodeString("a"), a);
        
        sendPacket(addr, m);

        Logger.getLogger("").log(Level.INFO, "Using DHT bootstrap node: {0}", addr);
    }

    /**
     * Requests the list of peers for a specific info hash from a contact in the DHT network.
     *
     * @param c The contact to request peers from.
     * @throws IOException If there is an I/O error while sending the request packet.
     */
    private void getPeers(Contact c) throws IOException {
        BencodeMap m = new BencodeMap();
        m.put(new BencodeString("t"), new BencodeString("get_peers"));
        m.put(new BencodeString("y"), new BencodeString("q"));
        m.put(new BencodeString("q"), new BencodeString("get_peers"));
        BencodeMap a = new BencodeMap();
        a.put(new BencodeString("id"), id);
        a.put(new BencodeString("info_hash"), searchID);
        a.put(new BencodeString("want"), new BencodeString("n6"));
        m.put(new BencodeString("a"), a);

        sendPacket(c.getAddr(), m);
    }

    /**
     * Announces the presence of a peer to the DHT network.
     *
     * @param c The contact representing the target peer.
     * @param token The token required for the announcement.
     * @throws IOException If there is an I/O error while sending the announcement packet.
     */
    private void announcePeer(Contact c, BencodeString token) throws IOException {
        if (token == null) {
            return;
        }

        BencodeMap m = new BencodeMap();
        m.put(new BencodeString("t"), new BencodeString("announce"));
        m.put(new BencodeString("y"), new BencodeString("q"));
        m.put(new BencodeString("q"), new BencodeString("announce_peer"));
        BencodeMap a = new BencodeMap();
        a.put(new BencodeString("id"), id);
        a.put(new BencodeString("info_hash"), searchID);
        a.put(new BencodeString("implied_port"), new BencodeInt(0));
        a.put(new BencodeString("port"), new BencodeInt(connectionManager.getServerPort()));
        a.put(new BencodeString("token"), token);
        m.put(new BencodeString("a"), a);

        sendPacket(c.getAddr(), m);
    }
    
    /**
     * Performs bootstrap operation by pinging DHT bootstrap nodes to join the DHT network.
     * Uses a list of predefined DHT bootstrap nodes for initialization.
     */
    private void bootstrap() {
        for (String dhtNode : DHT_BOOTSTRAP_NODES) {
            String[] addressParts = dhtNode.split(":");
            try {
                ping(new InetSocketAddress(addressParts[0], Integer.parseInt(addressParts[1])));
            } catch (IOException | NumberFormatException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * This method processes peers in the DHT to find the best contacts given certain conditions.
     * These conditions include limits on the number of "bad" contacts (those that are down or unavailable), 
     * and may also consider other factors such as distance from the current node (see peerQueue).
     */
    private void processPeers() {
        try {
            Vector<Contact> best = new Vector<Contact>();
            {
                Contact contact;
                while (best.size() < MAX_GOOD && null != (contact = peerQueue.poll())) {
                    if (!best.contains(contact) && getBad(contact) < MAX_BAD) {
                        best.add(contact);
                    }
                }
            }
            for (Contact contact : best) {
                getPeers(contact);
                makeBad(contact);
                addQueue(contact, false);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Sends a BencodeObject as a packet to the specified SocketAddress.
     *
     * @param addr The destination SocketAddress to send the packet to.
     * @param o The BencodeObject to be sent as a packet.
     * @throws IOException If an I/O error occurs during the sending process.
     */
    private void sendPacket(SocketAddress addr, BencodeObject o) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        o.write(out);
        byte[] buf = out.toByteArray();
        DatagramPacket p = new DatagramPacket(buf, buf.length, addr);
        dSock.send(p);
    }

    /**
     * Removes a contact from the list of bad peers, marking it as a good peer.
     *
     * @param c The contact to be marked as a good peer.
     */
    private void makeGood(Contact c) {
        peerBad.remove(c);
    }

    /**
     * Retrieves the bad count for a given contact, indicating its quality as a peer.
     *
     * @param c The contact for which the bad count is retrieved.
     * @return The bad count for the contact (0 if no bad count is recorded).
     */
    private int getBad(Contact c) {
        Integer i = peerBad.get(c);
        if (i == null) {
            return 0;
        }
        return i;
    }

    /**
     * Increases the bad count for a contact, marking it as a potentially bad peer.
     *
     * @param c The contact to be marked as a bad peer.
     */
    private void makeBad(Contact c) {
        peerBad.put(c, getBad(c) + 1);
    }

    /**
     * Adds a contact to the peer queue and optionally marks it as a good peer.
     *
     * @param c The contact to be added to the peer queue.
     * @param good Indicates whether the contact should be marked as a good peer.
     */
    private void addQueue(Contact c, boolean good) {
        if (!peerQueue.contains(c)) {
            peerQueue.add(c);
        }
        if (good) {
            makeGood(c);
        }
    }

    /**
     * Processes an incoming DatagramPacket containing Bencode-encoded data.
     *
     * @param packet The DatagramPacket containing the Bencode-encoded data.
     */
    private void receivePacket(DatagramPacket packet) {
        try {
            BencodeObject object = Bencode.parseBencode(new ByteArrayInputStream(packet.getData(), 0, packet.getLength()));

            if (((BencodeMap) object).get(new BencodeString("y")).equals(new BencodeString("r"))) {
                String transactionID = ((BencodeMap) object).get(new BencodeString("t")).toString();
                BencodeMap response = (BencodeMap) ((BencodeMap) object).get(new BencodeString("r"));

                switch (transactionID) {
                    case "ping":
                        handlePing(packet, response);
                        break;
                    case "get_peers":
                        handlePeers(packet, response);
                        break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handles a ping response received as a BencodeMap.
     *
     * @param packet   The DatagramPacket containing the ping response data.
     * @param response The BencodeMap representing the ping response.
     * @throws IOException If there is an I/O error during the handling process.
     */
    private void handlePing(DatagramPacket packet, BencodeMap response) throws IOException {
        BencodeString nodeId = (BencodeString) response.get(new BencodeString("id"));
        addQueue(new Contact(nodeId.getBytes(), packet.getSocketAddress()), true);
    }

    /**
     * Handles a get_peers response received as a BencodeMap, processing information about peers and tokens.
     *
     * @param packet   The DatagramPacket containing the get_peers response data.
     * @param response The BencodeMap representing the get_peers response.
     * @throws IOException If there is an I/O error during the handling process.
     */
    private void handlePeers(DatagramPacket packet, BencodeMap response) throws IOException {
        BencodeString nodeId = (BencodeString) response.get(new BencodeString("id"));
        Contact remotePeer = new Contact(nodeId.getBytes(), packet.getSocketAddress());
        if (maliciousPeers.contains(remotePeer)) {
            return;
        }
        addQueue(remotePeer, true);

        BencodeString nodes = (BencodeString) response.get(new BencodeString("nodes"));
        BencodeString nodes6 = (BencodeString) response.get(new BencodeString("nodes6"));
        BencodeList values = (BencodeList) response.get(new BencodeString("values"));

        if (nodes != null) {
            byte[] byteSequence = nodes.getBytes();
            for (int i = 0; i + Contact.CONTACT_ID_LEN + Contact.IPV4_LEN < byteSequence.length; i += Contact.CONTACT_ID_LEN + Contact.IPV4_LEN) {
                addQueue(new Contact(byteSequence, i, Contact.CONTACT_ID_LEN + Contact.IPV4_LEN), false);
            }
        }

        if (nodes6 != null) {
            byte[] byteSequence = nodes6.getBytes();
            for (int i = 0; i + Contact.CONTACT_ID_LEN + Contact.IPV6_LEN < byteSequence.length; i += Contact.CONTACT_ID_LEN + Contact.IPV6_LEN) {
                addQueue(new Contact(byteSequence, i, Contact.CONTACT_ID_LEN + Contact.IPV6_LEN), false);
            }
        }

        if (values != null) {
            for (BencodeObject val : values) {
                byte[] byteSequence = ((BencodeString) val).getBytes();
                InetSocketAddress addr = Contact.parseSocketAddress(byteSequence, 0, byteSequence.length);

                boolean ipAdded = false;
                for (Endpoint endPoint : connectionManager.getConnector().getIPs()) {
                    if (endPoint.getInetAddress().equals(addr.getAddress()) && endPoint.getPort() == addr.getPort()) {
                        ipAdded = true;
                        break;
                    }
                }

                if (!ipAdded && doPeerChecks(remotePeer, addr)) {
                    connectionManager.getConnector().addIP(addr.getAddress(),
                        addr.getPort(), null, "BitTorrent DHT", false);
                }
            }
        }
        
        if (announcePeerLastTime.size() > ANNOUNCE_PEER_CACHE_MAX) {
            announcePeerLastTime.clear();
        }

        long currentTimeMs = System.currentTimeMillis();
        BencodeString token = (BencodeString) response.get(new BencodeString("token"));
        if (!announcePeerLastTime.containsKey(remotePeer) || 
            announcePeerLastTime.get(remotePeer) + PEER_ANNOUNCE_INTERVAL_MS < currentTimeMs) {
            announcePeerLastTime.put(remotePeer, currentTimeMs);
            announcePeer(remotePeer, token);
        }
    }

    /**
     * Some peers from the DHT network take your info hash and spam addresses with the same port. 
     * The algorithm treats such peers as malicious and blocks them.
     *
     * @param remotePeer The Contact information.
     * @param address The InetSocketAddress sent by this contact.
     * 
     * @return false if the peer was blocked, otherwise returns true
     */
    private boolean doPeerChecks(Contact remotePeer, InetSocketAddress address) {
        boolean result = true;

        if (maliciousPeerCounts.size() > SUSPICIOUS_PEERS_MAX) {
            maliciousPeerCounts.clear();
            maliciousPeerAddresses.clear();
        }

        if (lastRemotePeer != null && 
            lastAddress != null && 
            lastAddress.getPort() == address.getPort() && 
            lastRemotePeer.equals(remotePeer)) {
            if (maliciousPeerCounts.containsKey(remotePeer)) {
                int current = maliciousPeerCounts.get(remotePeer);
                if (current > MAX_DHT_PEER_SPAM_ATTEMPTS) {
                    Logger.getLogger("").log(Level.INFO,
                        String.format("Detected malicious DHT peer %s.", remotePeer.toString()));
                    maliciousPeers.add(remotePeer);
                    maliciousPeerCounts.remove(remotePeer);

                    result = false;

                    Vector<InetSocketAddress> addrToRemove = new Vector<InetSocketAddress>();
                    for (InetSocketAddress maliciousPeerAddr : maliciousPeerAddresses.keySet()) {
                        if (maliciousPeerAddresses.get(maliciousPeerAddr).equals(remotePeer)) {
                            connectionManager
                                .getConnector()
                                .removeObsoleteIP(maliciousPeerAddr.getAddress(),
                                    maliciousPeerAddr.getPort(),
                                    "BitTorrent DHT");
                            addrToRemove.add(maliciousPeerAddr);
                        }
                    }
                    for (InetSocketAddress removeAddr : addrToRemove) {
                        maliciousPeerAddresses.remove(removeAddr);
                    }

                } else {
                    maliciousPeerCounts.put(remotePeer, current + 1);
                    maliciousPeerAddresses.put(address, remotePeer);
                }
            } else {
                maliciousPeerCounts.put(remotePeer, 1);
                maliciousPeerAddresses.put(address, remotePeer);
            }
        }

        lastRemotePeer = remotePeer;
        lastAddress = address;

        return result;
    }

    /**
     * Runs a thread to continuously receive UDP packets and process them.
     * 
     * This method runs a thread that listens for UDP packets, processes each packet,
     * and handles any exceptions related to I/O operations. The thread runs until
     * the flag `receivingThreadRunning` is set to false.
     */
    private void receivingThread() {
        try {
            byte[] buf = new byte[PACKET_LEN];
            DatagramPacket packet = new DatagramPacket(buf, PACKET_LEN);
            while (receivingThreadRunning) {
                dSock.receive(packet);
                receivePacket(packet);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
