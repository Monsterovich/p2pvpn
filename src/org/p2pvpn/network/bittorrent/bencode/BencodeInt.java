/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.network.bittorrent.bencode;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author wolfgang
 */
public class BencodeInt implements BencodeObject {

    int i;

    public BencodeInt(int i) {
        this.i = i;
    }

    public int getInt() {
        return i;
    }

    @Override
    public void write(OutputStream out) throws IOException {
        out.write('i');
        out.write(Integer.toString(i).getBytes());
        out.write('e');
    }

    @Override
    public String toString() {
        return Integer.toString(i);
    }
}
