/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.tuntap;

import java.io.File;
import java.nio.file.Paths;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * An abstract class for a virtual network adapter. 
 * Each operating system requires a different implementation.
 *
 * @author Wolfgang Ginolas
 */
public abstract class TunTap {

    private byte[] ip = null;

    /**
     * Load a libary (*.so or *.dll).
     *
     * @param libs the libary names
     * @throws java.io.IOException
     */
    static void loadLib(String... libs) throws Throwable {
        Throwable e = null;
        String clibPath = "";
        if (System.getenv().containsKey("CLIB_PATH")) {
            clibPath = System.getenv("CLIB_PATH");
        }
        for (String lib : libs) {
            try {
                File file = new File(Paths.get(clibPath, lib).toUri());
                if (file.exists()) {
                    System.load(file.getCanonicalPath());
                } else {
                    throw new Exception(String.format("loadLib failed. Library file %s is not found!", lib));
                }
                break;
            } catch (Exception eio) {
                e = eio;
            }
        }
        if (e != null) {
            throw e;
        }
    }

    /**
     * Return a TunTap object for the currently used operating system.
     *
     * @return the TunTap object
     * @throws java.lang.Exception
     */
    static public TunTap createTunTap() throws Exception {
        String osName = System.getProperty("os.name");

        if (osName.startsWith("Windows")) {
            return new TunTapWindows();
        } else if (osName.equals("Linux") || osName.equals("FreeBSD")) {
            return new TunTapLinux();
        } else {
            throw new Exception("The operating system " + osName + " is not supported!");
        }
    }

    /**
     * @return the name of the virtual network device
     */
    abstract public String getDev();

    /**
     * Close the device.
     */
    abstract public void close();

    /**
     * Send a packet to the virtual network adapter.
     *
     * @param b the packet
     * @param len the length of the packet
     */
    abstract public void write(byte[] b, int len);

    /**
     * Read a packet from the virtual network adapter.
     *
     * @param b the packet
     * @return length if the packet
     */
    abstract public int read(byte[] b);

    /**
     * Set the IP address of the virtual network adapter.
     *
     * @param ip the IP
     * @param subnetmask the subnet mask
     */
    public void setIP(String ip, String subnetmask) {
        try {
            this.ip = InetAddress.getByName(ip).getAddress();
        } catch (UnknownHostException ex) {
        }
    }

    /**
     * Return the last set IP address.
     *
     * @return the ip address
     */
    public byte[] getIPBytes() {
        return ip;
    }

}
