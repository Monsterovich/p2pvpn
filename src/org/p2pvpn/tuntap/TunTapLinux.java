/*
    Copyright 2008, 2009 Wolfgang Ginolas
    Copyright 2023-2025 Nikolay Borodin <Monsterovich>

    This file is part of Lanemu.

    Lanemu is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Lanemu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Lanemu.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.p2pvpn.tuntap;

import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The TunTap class for linux.
 *
 * @author Wolfgang Ginolas
 */
public class TunTapLinux extends TunTap {

    static {
        String libname = null;
        try {
            switch (System.getProperty("os.arch")) {
                case "x86":
                case "i386":
                case "i486":
                case "i586":
                case "i686":
                    libname = "clib/libTunTapLinux.so";
                    break;
                case "x86_64":
                case "amd64":
                    libname = "clib/libTunTapLinux64.so";
                    break;
                default:
                    Logger.getLogger("").log(Level.WARNING, "Unknown os.arch. Still trying to load libTunTapLinuxUnknown.so");
                    libname = "clib/libTunTapLinuxUnknown.so";
                    break;
            }
            loadLib(libname);
        } catch (Throwable e) {
            Logger.getLogger("").log(Level.SEVERE, "Could not load tap driver library", e);
        }
    }

    private int fd;
    private String dev;

    /**
     * Create a new TunTapLinux
     *
     * @throws java.lang.Exception
     */
    public TunTapLinux() throws Exception {
        if (1 == openTun()) {
            throw new Exception("Could not open '/dev/net/tun!'\n"
                + "Please run Lanemu as root.\n" +
                "Or alternatively, set CAP_NET_ADMIN capability via \"sudo setcap cap_net_admin=eip /path/to/java\".");
        }
    }

    @Override
    public String getDev() {
        return dev;
    }

    private native int openTun();

    @Override
    public native void close();

    @Override
    public native void write(byte[] b, int len);

    @Override
    public native int read(byte[] b);

    @Override
    public void setIP(String ip, String subnetmask) {
        super.setIP(ip, subnetmask);
        try {
            Process p = Runtime.getRuntime().exec(String.format("ifconfig %s %s netmask %s", dev, ip, subnetmask));
            p.waitFor();
            int result = p.exitValue();
            if (result == 255) {
                String makerootBin;
                if (System.getenv().containsKey("MAKEROOT_BIN")) {
                    makerootBin = System.getenv("MAKEROOT_BIN");
                } else {
                    makerootBin = GraphicsEnvironment.isHeadless() ? "sudo" : "pkexec";
                }
                p = Runtime.getRuntime().exec(String.format("%s ifconfig %s %s netmask %s", 
                    makerootBin, dev, ip, subnetmask));
                p.waitFor();
                result = p.exitValue();
            }

            if (result == 0) {
                Logger.getLogger("").log(Level.INFO, "IP set successfully");
            } else {
                Logger.getLogger("").log(Level.WARNING, "Could not set IP!");
            }
        } catch (IOException | InterruptedException e) {
            Logger.getLogger("").log(Level.WARNING, "Could not set IP!", e);
        }
    }

}
